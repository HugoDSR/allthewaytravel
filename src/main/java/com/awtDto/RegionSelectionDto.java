package com.awtDto;

public class RegionSelectionDto {

    String region;
    boolean selected;

    public RegionSelectionDto(String region, boolean selected) {
        this.region = region;
        this.selected = selected;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
