package com.awtDto;

public class CityWeatherDto {
    int weekDay, temperature;
    String weatherType, weekDayVal;

    public CityWeatherDto(int weekDay, int temperature, String weatherType) {
        this.weekDay = weekDay;
        this.temperature = temperature;
        this.weatherType = weatherType;

        switch (weekDay){
            case 1:
                weekDayVal = "dom.";
                break;
            case 2:
                weekDayVal = "seg.";
                break;
            case 3:
                weekDayVal = "ter.";
                break;
            case 4:
                weekDayVal = "qua.";
                break;
            case 5:
                weekDayVal = "qui.";
                break;
            case 6:
                weekDayVal = "sex.";
                break;
            case 7:
                weekDayVal = "sab.";
                break;

        }
    }

    public int getWeekDay() {
        return weekDay;
    }

    public int getTemperature() {
        return temperature;
    }

    public String getWeatherType() {
        return weatherType;
    }

    public String getWeekDayVal() {
        return weekDayVal;
    }


}
