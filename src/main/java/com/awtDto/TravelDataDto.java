package com.awtDto;

import javax.persistence.*;

@Entity
@Table(name = "traveldata")
public class TravelDataDto {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    int clients;
    int voos;
    int viagens;
    int cruzeiros;

    public void setId(Integer id) {
        this.id = id;
    }

    public void setClients(int clients) {
        this.clients = clients;
    }

    public void setVoos(int voos) {
        this.voos = voos;
    }

    public void setViagens(int viagens) {
        this.viagens = viagens;
    }

    public void setCruzeiros(int cruzeiros) {
        this.cruzeiros = cruzeiros;
    }

    public Integer getId() {
        return id;
    }

    public int getClients() {
        return clients;
    }

    public int getVoos() {
        return voos;
    }

    public int getViagens() {
        return viagens;
    }

    public int getCruzeiros() {
        return cruzeiros;
    }
}
