package com.awtDto;

import javax.persistence.*;

@Entity
@Table(name = "testemony")
public class TestemonyDto {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    private String content;
    private String author;
    private boolean isactive;

    public Integer getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public String getAuthor() {
        return author;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isIsactive() {
        return isactive;
    }

    public void setIsactive(boolean isactive) {
        this.isactive = isactive;
    }
}
