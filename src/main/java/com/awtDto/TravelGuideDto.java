package com.awtDto;

import javax.persistence.*;

@Entity
@Table(name = "travelguide")
public class TravelGuideDto {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    private String worldarea;
    private String country;
    private String city;
    private String imagem;
    private boolean isactive;

    public String getWorldarea() {
        return worldarea;
    }

    public void setWorldarea(String worldarea) {
        this.worldarea = worldarea;
    }



    public boolean isIsactive() {
        return isactive;
    }

    public void setIsactive(boolean isactive) {
        this.isactive = isactive;
    }

    public Integer getId() {
        return id;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getImagem() {
        return imagem;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }
}
