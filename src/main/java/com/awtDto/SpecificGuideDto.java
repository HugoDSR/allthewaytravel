package com.awtDto;

import javax.persistence.*;

@Entity
@Table(name = "specificguide")
public class SpecificGuideDto {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    private String nome;
    private String descricao;
    private String moeda;
    private String moedasimbolo;
    private String tempo;
    private String horas;
    private String quandoir;
    private String link;
    private String imagem;
    private boolean isactive;
    private String coordinates;
    private int weatherID;

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setMoeda(String moeda) {
        this.moeda = moeda;
    }

    public void setMoedasimbolo(String moedasimbolo) {
        this.moedasimbolo = moedasimbolo;
    }

    public void setTempo(String tempo) {
        this.tempo = tempo;
    }

    public void setHoras(String horas) {
        this.horas = horas;
    }

    public void setQuandoir(String quandoir) {
        this.quandoir = quandoir;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public Integer getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getMoeda() {
        return moeda;
    }

    public String getMoedasimbolo() {
        return moedasimbolo;
    }

    public String getTempo() {
        return tempo;
    }

    public String getHoras() {
        return horas;
    }

    public String getQuandoir() {
        return quandoir;
    }

    public String getImagem() {
        return imagem;
    }

    public String getLink() {
        return link;
    }

    public boolean isIsactive() {
        return isactive;
    }

    public void setIsactive(boolean isactive) {
        this.isactive = isactive;
    }

    public int getWeatherID() {
        return weatherID;
    }

    public void setWeatherID(int weatherID) {
        this.weatherID = weatherID;
    }

    @Override
    public String toString() {
        return "SpecificGuideDto{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", descricao='" + descricao + '\'' +
                ", moeda='" + moeda + '\'' +
                ", moedasimbolo='" + moedasimbolo + '\'' +
                ", tempo='" + tempo + '\'' +
                ", horas='" + horas + '\'' +
                ", quandoir='" + quandoir + '\'' +
                ", link='" + link + '\'' +
                ", imagem='" + imagem + '\'' +
                ", isactive=" + isactive +
                ", coordinates='" + coordinates + '\'' +
                ", weatherID=" + weatherID +
                '}';
    }
}
