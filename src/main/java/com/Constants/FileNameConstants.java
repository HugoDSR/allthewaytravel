package com.Constants;

public class FileNameConstants {

    public static String BASE_HEADER_LOGO = "/images/ATWT-Logo.png";
    public static String BASIC_LOGO = "/images/allthewaytravel.svg";
    public static String BASE_HEADER_FACEBOOK = "/images/Facebook.png";
    public static String    BASE_FOOTER_LOGOS = "/images/Logos-Parceiros.png";

    public static String MAIN_DESTINATIONS_1 = "/images/Homepage-Maldivas.jpg";
    public static String MAIN_DESTINATIONS_2 = "/images/Homepage-Escapadinhas(Veneza).jpg";
    public static String MAIN_DESTINATIONS_3 = "/images/Homepage-Cruzeiros.jpg";
    public static String WORLD_MAP = "/images/mapa-mundo-bege.png";

    public static String ABOUT_US = "/images/Sobre nós.jpg";
    public static String ABOUT_US_VIAGENS = "/images/icon_sobrenos_atwt_1.png";
    public static String ABOUT_US_CRUZEIROS = "/images/icon_sobrenos_atwt_2.png";
    public static String ABOUT_US_VOOS = "/images/icon_sobrenos_atwt_8.png";
    public static String ABOUT_US_CLIENTES = "/images/icon_sobrenos_atwt_9.png";

    public static String SERVICES_TRAVEL = "/images/icon_sobrenos_atwt_3.png";
    public static String SERVICES_DOCUMENTS = "/images/icon_sobrenos_atwt_4.png";
    public static String SERVICES_OTHER = "/images/icon_sobrenos_atwt_5.png";
    public static String SERVICES_ASSISTANCE = "/images/icon_sobrenos_atwt_6.png";
    public static String SERVICES_TAILORTRAVEL = "/images/icon_sobrenos_atwt_7.png";
    public static String SERVICES_COMPLAINTS = "/images/icon_sobrenos_atwt_10.png";
    public static String SERVICES_GDPR = "/images/icon_sobrenos_atwt_11.png";
    public static String SERVICES_NORMALIZEDFORM = "/images/icon_sobrenos_atwt_12.png";

    public static String TRAVEL_GUIDES_= "/images/Guias_de_Viagem.jpg";
    public static String TRAVEL_GUIDES_MAP = "/images/mapa-mundo-azul.png";

    public static String GUIDES_ICON_NOTE = "/images/iconbanknote.png";
    public static String GUIDES_ICON_CLOUD = "/images/iconcloud.png";
    public static String GUIDES_ICON_TIME = "/images/icontime.png";
    public static String GUIDES_ICON_GLOBE = "/images/iconglobe.png";

    public static String CONTACT_US = "/images/Contactos.jpg";

    public static String COVIDDIAGRAM = "/images/Covid-diagram.png";

    public static String

            FACEBOOK_PIXEL_CODE = "https://www.facebook.com/tr?id=486384455187909&ev=PageView&noscript=1";
}
