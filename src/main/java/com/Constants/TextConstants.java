package com.Constants;

public class TextConstants {

    public static String ABOUT_US_MAIN_TEXT_P1 = "A All The Way nasce de de uma mão cheia de profissionais motivados para dar"
            +" continuidade à experiência e ao trabalho desenvolvido ao longo do tempo, com cada um dos nossos prezados clientes.";
    public static String ABOUT_US_MAIN_TEXT_P2 = "Na nossa agência tratamos de viagens diariamente, de e para qualquer lugar onde "
            +"seja essencial estar. Estamos preparados para aconselhar de forma transparente e com o máximo rigor, tendo como base "
            +"o binómio qualidade/preco.";
    public static String ABOUT_US_MAIN_TEXT_P3 = "Integrados em organizaçōes do setor português tão prestigiantes, como: APAVT, IATA, "
            +"Provedor de Clientes das Agências de Viagens e Turismo de Portugal fazem com que, a nossa responsabilidade não "
            +"só como empresários mas como cidadãos nacionais, nos transporte além-fronteiras, com a honra de sermos portugueses.";
    public static String ABOUT_US_MAIN_TEXT_P4 = "All The Way é o nosso lema e cumpre o nosso propósito: a excelência";

    public static String TOUR_GUIDES_TEXT = "Preparámos para si as melhores dicas para visitar no seu próximo destino";

    public static String BUDGET_TEXT = "Está a organizar a sua próxima viagem? Faça o pedido de orçamento da sua viagem "
            +"sem compromisso. Com a All The Way Travel poderá poupar tempo na organização da sua viagem, contará com vantagens "
            +"exclusivas e terá um orçamento personalizado.";
}
