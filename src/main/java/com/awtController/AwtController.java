package com.awtController;

import com.awtDao.*;
import com.awtDto.*;
import com.awtServices.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.*;

import static com.Constants.FileNameConstants.*;
import static com.Constants.TextConstants.*;

@Controller
public class AwtController {


    @Autowired
    private Environment env;

    @Autowired
    TestemonyService testemonyService;
    @Autowired
    DestinationService destinationService;
    @Autowired
    TravelDataService travelDataService;
    @Autowired
    TravelGuideService travelGuideService;
    @Autowired
    SpecificGuideService specificGuideService;
    @Autowired
    SpecificGuideLocationService specificGuideLocationService;
    @Autowired
    ImageHandlerService imageHandlerService;
    @Autowired
    TestingService testingService;
    @Autowired
    ContactUsService contactUsService;

    @Autowired
    private TestemonyDao testemonyDao;
    @Autowired
    private TravelGuideDao travelGuideDao;
    @Autowired
    private DestinationDao destinationDao;
    @Autowired
    private SpecificGuideDao specificGuideDao;
    @Autowired
    private SpecificGuideLocationDao specificGuideLocationDao;
    @Autowired
    private TravelDataDao travelDataDao;
    @Autowired
    private UserDao userDao;

    private boolean firstRun = true;

    public void reminder() {
        Timer timer = new Timer();
        timer.schedule(new RemindTask(travelDataDao, travelDataService,"clientes"), 0l, 60*60*1000);
        timer.schedule(new RemindTask(travelDataDao, travelDataService, "voos"), 0l, 5*60*1000);
        timer.schedule(new RemindTask(travelDataDao, travelDataService, "viagens"), 0l, 35*60*1000);
        timer.schedule(new RemindTask(travelDataDao, travelDataService, "cruzeiros"), 0l, 751*60*1000);
    }

    static class RemindTask extends TimerTask {
        TravelDataDao travelDataDao;
        TravelDataService travelDataService;
        String field;
        public RemindTask(TravelDataDao travelDataDao, TravelDataService travelDataService, String field){
            this.travelDataDao = travelDataDao;
            this.field = field;
            this.travelDataService = travelDataService;
        }
        public void run() {
            travelDataService.updateTravelData(field, travelDataDao);
        }
    }

    @RequestMapping({"/", "/home"})
    public String greeting(Model model) {
        model.addAttribute("awtLogo", BASE_HEADER_LOGO);
        model.addAttribute("facebookLogo", BASE_HEADER_FACEBOOK);
        model.addAttribute("parceriosLogos", BASE_FOOTER_LOGOS);
        model.addAttribute("mainDestinations1", MAIN_DESTINATIONS_1);
        model.addAttribute("mainDestinations2", MAIN_DESTINATIONS_2);
        model.addAttribute("mainDestinations3", MAIN_DESTINATIONS_3);
        model.addAttribute("worldMapDivider", WORLD_MAP);
        model.addAttribute("testemonies", testemonyService.getTestemonies(testemonyDao));
        model.addAttribute("destinations", destinationService.getDestinations(destinationDao));
        model.addAttribute("travelGuides", travelGuideService.getTravelGuides(travelGuideDao));
        model.addAttribute("facebookPixelCode", FACEBOOK_PIXEL_CODE);
        if (firstRun){
            reminder();
            System.out.println("Iniciar contadores");
            firstRun = false;
        }
        return "home";
    }

    @RequestMapping("/sobrenos")
    public String sobreNos(Model model) {
        model.addAttribute("awtLogo", BASE_HEADER_LOGO);
        model.addAttribute("facebookLogo", BASE_HEADER_FACEBOOK);
        model.addAttribute("parceriosLogos", BASE_FOOTER_LOGOS);
        model.addAttribute("aboutUs", ABOUT_US);
        model.addAttribute("aboutUsClientes", ABOUT_US_CLIENTES);
        model.addAttribute("aboutUsVoos", ABOUT_US_VOOS);
        model.addAttribute("aboutUsViagens", ABOUT_US_VIAGENS);
        model.addAttribute("aboutUsCruzeiros", ABOUT_US_CRUZEIROS);
        model.addAttribute("aboutUsData", travelDataService.getTravelData(travelDataDao));
        model.addAttribute("worldMapDivider", WORLD_MAP);
        model.addAttribute("aboutUsText1", ABOUT_US_MAIN_TEXT_P1);
        model.addAttribute("aboutUsText2", ABOUT_US_MAIN_TEXT_P2);
        model.addAttribute("aboutUsText3", ABOUT_US_MAIN_TEXT_P3);
        model.addAttribute("aboutUsText4", ABOUT_US_MAIN_TEXT_P4);
        model.addAttribute("serviceTravel", SERVICES_TRAVEL);
        model.addAttribute("serviceDocuments", SERVICES_DOCUMENTS);
        model.addAttribute("serviceOther", SERVICES_OTHER);
        model.addAttribute("serviceAssistance", SERVICES_ASSISTANCE);
        model.addAttribute("serviceTailorTravel", SERVICES_TAILORTRAVEL);
        model.addAttribute("serviceComplaintBook", SERVICES_COMPLAINTS);
        model.addAttribute("serviceClientProtection", SERVICES_GDPR);
        model.addAttribute("servicesNormalizedForm", SERVICES_NORMALIZEDFORM);
        model.addAttribute("facebookPixelCode", FACEBOOK_PIXEL_CODE);
        return "sobreNos";
    }

    @RequestMapping("/guias")
    public String travelGuides(@RequestParam(value = "regioes", required = false) String regioes, Model model) {
        List<String> regions = new ArrayList<>();
        if(regioes != null) {
            regions = Arrays.asList(regioes.split("-"));
        }
        model.addAttribute("awtLogo", BASE_HEADER_LOGO);
        model.addAttribute("facebookLogo", BASE_HEADER_FACEBOOK);
        model.addAttribute("parceriosLogos", BASE_FOOTER_LOGOS);
        model.addAttribute("travelguide_message", TOUR_GUIDES_TEXT);
        model.addAttribute("travelguides", TRAVEL_GUIDES_);
        model.addAttribute("travel_map", TRAVEL_GUIDES_MAP);
        model.addAttribute("oldRegions", regioes);
        model.addAttribute("travelOptions", travelGuideService.getRegionSelection(travelGuideDao, regions));
        model.addAttribute("travelguides_cards", travelGuideService.getFilteredTravelGuides(travelGuideDao, regions));
        model.addAttribute("facebookPixelCode", FACEBOOK_PIXEL_CODE);

        return "travelGuide";
    }

    @PostMapping("/guides/region")
    public String setRegions(@RequestParam("filterVal") String selectedRegions){
        String[] regions = selectedRegions.split("-");
        String result = "";
        for( String region : regions) {
            result += region + "-";
        }
        return "redirect:/guias?regioes="+result;
    }

    @PostMapping("/guides/email")
    public String setEmail(){
        return "redirect:/guias";
    }

    @RequestMapping("/guia")
    public String specificGuide(@RequestParam("cidade") String cidadeName, Model model) {
        model.addAttribute("awtLogo", BASE_HEADER_LOGO);
        model.addAttribute("facebookLogo", BASE_HEADER_FACEBOOK);
        model.addAttribute("parceriosLogos", BASE_FOOTER_LOGOS);
        model.addAttribute("guideDto", specificGuideService.getSpecificGuide(specificGuideDao, cidadeName));
        model.addAttribute("iconNote", GUIDES_ICON_NOTE);
        model.addAttribute("iconCloud", GUIDES_ICON_CLOUD);
        model.addAttribute("iconTime", GUIDES_ICON_TIME);
        model.addAttribute("iconGlobe", GUIDES_ICON_GLOBE);
        model.addAttribute("travelGuideLocations", specificGuideLocationService.getSpecificGuideLocations(specificGuideLocationDao, cidadeName));
        model.addAttribute("travelGuideLocationsCoordinates", specificGuideLocationService.getLocationCoordinates(specificGuideLocationDao, cidadeName));
        model.addAttribute("travelGuideLocationWeather", specificGuideLocationService.getWeather(cidadeName, specificGuideDao));
        model.addAttribute("facebookPixelCode", FACEBOOK_PIXEL_CODE);
        return "specificTravelGuide";
    }


    @RequestMapping("/contactos")
    public String contactUs(Model model) {
        model.addAttribute("awtLogo", BASE_HEADER_LOGO);
        model.addAttribute("facebookLogo", BASE_HEADER_FACEBOOK);
        model.addAttribute("parceriosLogos", BASE_FOOTER_LOGOS);
        model.addAttribute("contactosTitle", CONTACT_US);
        model.addAttribute("orcamentoText", BUDGET_TEXT);
        model.addAttribute("facebookPixelCode", FACEBOOK_PIXEL_CODE);
        return "contactUs";
    }

    @RequestMapping(value = "/contactos", method = RequestMethod.POST)
    public String contactUsPost(@RequestParam("inputName") String name, @RequestParam("inputEmail") String email,
                                @RequestParam("inputDestination") String destination, @RequestParam("inputGoDateMonth") String gomonth,
                                @RequestParam("inputGoDateDay") String goday, @RequestParam("inputGoDateYear") String goyear,
                                @Autowired
                                @RequestParam("inputComeDateMonth") String comeMonth, @RequestParam("inputComeDateDay") String comeday,
                                @RequestParam("inputComeDateYear") String comeyear, @RequestParam("inputPassengerNumber") String passengerNumber,
                                @RequestParam("inputBudget") String budget, @RequestParam("inputObservations") String obsevation) {

        Map<String, String> emailContent = new HashMap<>();
        emailContent.put("name", name);
        emailContent.put("email", email);
        emailContent.put("destination", destination);
        emailContent.put("goMonth", gomonth);
        emailContent.put("goDay", goday);
        emailContent.put("goYear", goyear);
        emailContent.put("comeMonth", comeMonth);
        emailContent.put("comeDay", comeday);
        emailContent.put("comeYear", comeyear);
        emailContent.put("passengerNumber", passengerNumber);
        emailContent.put("budget", budget);
        emailContent.put("obsevation", obsevation);
        contactUsService.SendBudget(emailContent, env);
        return "redirect:/";
    }

    @RequestMapping("/dados")
    public String dataProtection(Model model) {
        model.addAttribute("awtLogo", BASE_HEADER_LOGO);
        model.addAttribute("basicLogo",BASIC_LOGO);
        model.addAttribute("facebookLogo", BASE_HEADER_FACEBOOK);
        model.addAttribute("facebookPixelCode", FACEBOOK_PIXEL_CODE);
        return "gdprPage";
    }

    @RequestMapping("/covid")
    public String covidData(Model model) {
        model.addAttribute("awtLogo", BASE_HEADER_LOGO);
        model.addAttribute("basicLogo",BASIC_LOGO);
        model.addAttribute("facebookLogo", BASE_HEADER_FACEBOOK);
        model.addAttribute("facebookPixelCode", FACEBOOK_PIXEL_CODE);
        model.addAttribute("covidDiagram", COVIDDIAGRAM);
        return "covidPage";
    }

    @RequestMapping("/fichainformativa")
    public String fichaInformativa(Model model) {
        model.addAttribute("awtLogo", BASE_HEADER_LOGO);
        model.addAttribute("basicLogo",BASIC_LOGO);
        model.addAttribute("facebookLogo", BASE_HEADER_FACEBOOK);
        model.addAttribute("facebookPixelCode", FACEBOOK_PIXEL_CODE);
        return "normalizedForm";
    }

    @RequestMapping(value = "/administrador")
    public String adminLoged(Model model) {
        model.addAttribute("basicLogo",BASIC_LOGO);
        model.addAttribute("facebookPixelCode", FACEBOOK_PIXEL_CODE);
        return "loginAdmin";
    }

    @RequestMapping(value = "/administrador", method = RequestMethod.POST)
    public String adminLogin(@RequestParam("username") String username, @RequestParam("password") String password) {
        System.out.println(username);
        System.out.println(password);
        if(username.equals("emilio") && password.equals("rocha")){
            return "redirect:/adminPannel";
        }
        return "loginAdmin";
    }

    @RequestMapping(value = "/adminAddDestination", method = RequestMethod.GET)
    public String adminDestination(Model model)
    {
        model.addAttribute("destination", destinationDao.findAll());
        return "adminDestination";
    }

    @RequestMapping(value = "/adminAddDestination", method = RequestMethod.POST)
    public String adminDestination(@RequestParam("nome") String nome,
                                   @RequestParam("descricao") String descricao,
                                   @RequestParam("link") String link,
                                   @RequestParam("preco") double preco,
                                   @RequestParam("pic") MultipartFile file) {
        DestinationDto result = new DestinationDto();
        result.setNome(nome);
        result.setDescricao(descricao);
        result.setLink(link);
        result.setPreco(preco);
        result.setImagem(imageHandlerService.saveImage(file));
        destinationDao.save(result);
        return "adminPannel";
    }
    @RequestMapping(value = "/adminAddSpecificGuide", method = RequestMethod.GET)
    public String adminSpecificGuide(Model model) {
        model.addAttribute("specificGuide",specificGuideDao.findAll());
        return "adminSpecificGuide";
    }

    @RequestMapping(value = "/adminAddSpecificGuide", method = RequestMethod.POST)
    public String adminSpecificGuide(@RequestParam("pais") String pais,
                                     @RequestParam("nome") String nome,
                                     @RequestParam("descricao") String descricao,
                                     @RequestParam("moeda") String moeda,
                                     @RequestParam("moedasimbolo") String moedasimbolo,
                                     @RequestParam("tempo") String tempo,
                                     @RequestParam("horas") String horas,
                                     @RequestParam("quandoir") String quandoir,
                                     @RequestParam("link") String link,
                                     @RequestParam("pic") MultipartFile file,
                                     @RequestParam("poster-pic") MultipartFile file2){
        SpecificGuideDto result = new SpecificGuideDto();
        result.setNome(nome);
        result.setDescricao(descricao);
        result.setMoeda(moeda);
        result.setMoedasimbolo(moedasimbolo);
        result.setTempo(tempo);
        result.setHoras(horas);
        result.setQuandoir(quandoir);
        result.setLink(link);
        result.setImagem(imageHandlerService.saveImage(file));
        specificGuideDao.save(result);
        TravelGuideDto travelGuideResult = new TravelGuideDto();
        travelGuideResult.setCity(nome);
        travelGuideResult.setCountry(pais);
        travelGuideResult.setImagem(imageHandlerService.saveImage(file2));
        travelGuideDao.save(travelGuideResult);
        return "redirect:/adminPannel";
    }

    @RequestMapping(value = "/adminAddSpecificGuideLocation", method = RequestMethod.GET)
    public String adminSpecificGuideLocation(Model model) {
        specificGuideLocationDao.findAll();
        model.addAttribute("specificGuideLocation",specificGuideLocationDao.findAll());
        return "adminSpecificGuideLocation";
    }

    @RequestMapping(value = "/adminAddSpecificGuideLocation", method = RequestMethod.POST)
    public String adminSpecificGuideLocation(@RequestParam("cidade") String cidade,
                                    @RequestParam("nome") String nome,
                                    @RequestParam("descricao") String descricao,
                                    @RequestParam("pic") MultipartFile file) {
        SpecificGuideLocationDto result = new SpecificGuideLocationDto();
        result.setCity(cidade);
        result.setNome(nome);
        result.setDescricao(descricao);
        result.setImagem(imageHandlerService.saveImage(file));
        specificGuideLocationDao.save(result);
        return "redirect:/adminPannel";
    }

    @RequestMapping(value = "/adminAddTestemony", method = RequestMethod.GET)
    public String adminTestemony(Model model)
    {
        model.addAttribute("testemony", testemonyDao.findAll());
        return "adminTestemony";
    }

    @RequestMapping(value = "/adminAddTestemony", method = RequestMethod.POST)
    public String adminTestemony(@RequestParam("autor") String autor,
                                   @RequestParam("testemunho") String testemunho) {
        TestemonyDto result = new TestemonyDto();
        result.setAuthor(autor);
        result.setContent(testemunho);
        testemonyDao.save(result);
        return "redirect:/adminPannel";
    }

    @RequestMapping(value="/adminPannel", method = RequestMethod.GET)
    public String adminPannel(){
        return "adminPannel";
    }


    @RequestMapping(value="/load", method = RequestMethod.GET)
    public String loadFile(){
//        return imageHandlerService.changeImagesNames();
        return imageHandlerService.loadFile();
    }

    @RequestMapping(value="/testservice", method = RequestMethod.GET)
    public String testService() throws NoSuchProviderException, NoSuchAlgorithmException {
//        UserDto user = testingService.createUser("hugo.rocha", "mbbbggqv33");
//        userDao.save(user);

        UserDto postUser = userDao.findById(1);
        testingService.testHashing(postUser);
        return "redirect:/adminPannel";
    }
}