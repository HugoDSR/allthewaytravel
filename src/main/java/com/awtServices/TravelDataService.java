package com.awtServices;

import com.awtDao.TravelDataDao;
import com.awtDto.TravelDataDto;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class TravelDataService {

    public TravelDataDto getTravelData(TravelDataDao travelDataDao) {
        TravelDataDto result = travelDataDao.findById(1);
        return result;
    }

    public void updateTravelData(String field, TravelDataDao travelDataDao){
        TravelDataDto data = travelDataDao.findById(1);
        int count = 0;
        if(field.equalsIgnoreCase("clientes")){
            count = data.getClients();
            count = count + 1;
            data.setClients(count);
            travelDataDao.save(data);
        }if(field.equalsIgnoreCase("viagens")){
            count = data.getViagens();
            count = count + 1;
            data.setViagens(count);
            travelDataDao.save(data);
        }if(field.equalsIgnoreCase("voos")){
            count = data.getVoos();
            count = count + 1;
            data.setVoos(count);
            travelDataDao.save(data);
        }if(field.equalsIgnoreCase("cruzeiros")){
            count = data.getCruzeiros();
            count = count + 1;
            data.setCruzeiros(count);
            travelDataDao.save(data);
        }
    }
}