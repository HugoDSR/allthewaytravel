package com.awtServices;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class ImageHandlerService {

    public String saveImage(MultipartFile file) {
        try {
            File convFile = new File(file.getOriginalFilename());
            FileOutputStream fos = new FileOutputStream(convFile);
            fos.write(file.getBytes());
            fos.close();
//            ImageIO.write(scale(bufferedImage, 400,433), "jpg", newFile);
            saveImage(file.getOriginalFilename(), convFile);
        }catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return "https://atwt.s3.amazonaws.com/"+file.getOriginalFilename();
    }

    public BufferedImage scale(BufferedImage img, int targetWidth, int targetHeight) {

        int type = (img.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        BufferedImage ret = img;
        BufferedImage scratchImage = null;
        Graphics2D g2 = null;

        int w = img.getWidth();
        int h = img.getHeight();

        int prevW = w;
        int prevH = h;

        do {
            if (w > targetWidth) {
                w /= 2;
                w = (w < targetWidth) ? targetWidth : w;
            }

            if (h > targetHeight) {
                h /= 2;
                h = (h < targetHeight) ? targetHeight : h;
            }

            if (scratchImage == null) {
                scratchImage = new BufferedImage(w, h, type);
                g2 = scratchImage.createGraphics();
            }

            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                    RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g2.drawImage(ret, 0, 0, w, h, 0, 0, prevW, prevH, null);

            prevW = w;
            prevH = h;
            ret = scratchImage;
        } while (w != targetWidth || h != targetHeight);

        if (g2 != null) {
            g2.dispose();
        }
        if (targetWidth != ret.getWidth() || targetHeight != ret.getHeight()) {
            scratchImage = new BufferedImage(targetWidth, targetHeight, type);
            g2 = scratchImage.createGraphics();
            g2.drawImage(ret, 0, 0, null);
            g2.dispose();
            ret = scratchImage;
        }
        return ret;
    }

    public void saveImage(String fileObjKeyName, File file){
        String bucketName = "atwt";
        try {
            AmazonS3 s3Client = new AmazonS3Client();
            // Upload a file as a new object with ContentType and title specified.
            PutObjectRequest request = new PutObjectRequest(bucketName, fileObjKeyName, file);
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("plain/text");
            metadata.addUserMetadata("x-amz-meta-title", "someTitle");
            request.setMetadata(metadata);
            s3Client.putObject(request);
        }
        catch(AmazonServiceException e) {
            e.printStackTrace();
        }
        catch(SdkClientException e) {
            e.printStackTrace();
        }
    }

    public String loadFile(){
        BufferedReader br = null;
        FileReader fr = null;
        try {
//            fr = new FileReader("/home/hugo/IdeaProjects/allthewaytravel/src/main/resources/static/images/Soraia-Destinos.txt");
            fr = new FileReader("/home/hugo/IdeaProjects/allthewaytravel/src/main/resources/static/images/Soraia-locations.txt");
            br = new BufferedReader(fr);
            String sCurrentLine = br.readLine();
            String[] cities = sCurrentLine.split("<c>");

            String query = "INSERT INTO specificguidelocation (city, nome, descricao, imagem, isactive, coordinates) VALUES \n";
//            String query = "INSERT INTO specificguide (nome, descricao, moeda, moedasimbolo, tempo, horas, quandoir, link, imagem, isactive, coordinates) VALUES \n";
//            String query = "INSERT INTO travelguide (country, city, imagem, isactive) VALUES \n";

            for(String c : cities){
                String city = "", nome = "", description = "";
//                String pais = "", cidade = "", description = "";
                String[] locations = c.split("<nd>");
                for(int i=0; i< locations.length; i++){
                    if(i==0){
                        city = locations[i];
//                        pais = locations[i];
                    }
                    else if(i%2!=0){
                        nome = locations[i];
//                        cidade = locations[i];

//                        String imageName = "https://atwt.s3.amazonaws.com/ADICIONARCIDADE.jpg";
//                        query = query+ "(\""+pais+"\", \""+cidade+"\", \""+imageName.replace(" ", "")+"\""+",0),\n ";
                    }
                    else {
                        description = locations[i];
                        String imageName = "https://atwt.s3.amazonaws.com/"+city+"-"+nome+".jpg";
//                        String imageName = "https://atwt.s3.amazonaws.com/ADICIONARCIDADE.jpg";
                        query = query+ "(\""+city+"\", \""+nome+"\", \""+description.replace("\"","\\\"")+"\", \""+imageName.replace(" ", "")+"\""+",0,0),\n ";
//                        query = query+ "(\""+nome+"\", \""+description+"\", \"euro\",\"€\",\"Sol\",\"GMT+1\",\"Verão\",\"www.google.com\",\""+imageName.replace(" ", "")+"\""+",0,0),\n ";
                    }
                }
            }
            System.out.println(query);



        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
                if (fr != null)
                    fr.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            return "redirect:/adminPannel";
        }
    }

    public String changeImagesNames(){

        File folder = new File("/home/hugo/IdeaProjects/images/Soraia");
        File[] listOfFiles = folder.listFiles();
        try {

            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    System.out.println(listOfFiles[i].getName());
                    Path source = Paths.get("/home/hugo/IdeaProjects/images/Soraia/"+listOfFiles[i].getName());
                    Files.move(source, source.resolveSibling(listOfFiles[i].getName().replace(" ","")));
                } else if (listOfFiles[i].isDirectory()) {
                }
            }
        } catch (IOException e){
            e.getStackTrace();
        }

        return "redirect:/adminPannel";

    }

    public void downloadFromS3() throws IOException {
        AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();
        S3Object object = s3Client.getObject("atwt", "file-key.JPG");
        File file = new File("/Users/hugorocha/sandbox/allthewaytravel/src/main/resources/static/contentManagement/");
        FileUtils.copyInputStreamToFile(object.getObjectContent(), file);
    }

    private void displayTextInputStream(InputStream input) throws IOException {
        // Read the text input stream one line at a time and display each line.
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        String line = null;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
        System.out.println();
    }
}