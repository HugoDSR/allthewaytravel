package com.awtServices;

import com.awtDao.SpecificGuideDao;
import com.awtDao.SpecificGuideLocationDao;
import com.awtDto.CityWeatherDto;
import com.awtDto.SpecificGuideDto;
import com.awtDto.SpecificGuideLocationDto;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class SpecificGuideLocationService {

    public List<SpecificGuideLocationDto> getSpecificGuideLocations(SpecificGuideLocationDao specificGuideLocationDao, String city){
        return specificGuideLocationDao.findByCity(city);
    }

    public String getLocationCoordinates(SpecificGuideLocationDao specificGuideLocationDao, String city) {
        List<SpecificGuideLocationDto> locations = getSpecificGuideLocations(specificGuideLocationDao, city);
        String result = new String();
        for (SpecificGuideLocationDto location : locations){
            result += location.getCoordinates()+"*"+location.getNome() + " - ";
        }

        return result;
    }

    public List<CityWeatherDto> getWeather(String cityName, SpecificGuideDao specificGuideDao) {
        SpecificGuideDto city = specificGuideDao.findByNome(cityName);
        List<JSONObject> weatherPerDate = new ArrayList<>();
        List<CityWeatherDto> weatherPerCity = new ArrayList<>();
        String apiUrl = "http://api.openweathermap.org/data/2.5/forecast?id="+city.getWeatherID()+"&units=metric&appid=d479e3bd3a289e07fa1cccf1c3bb9490";
        try {
            RestTemplate restTemplate = new RestTemplate();
            String quote = restTemplate.getForObject(apiUrl, String.class);

            JSONArray arr = new JSONObject(quote).getJSONArray("list");
            Calendar c = Calendar.getInstance();
            for (int i = 0; i < arr.length(); i++) {
                JSONObject post_id = arr.getJSONObject(i);
                String jsonDate = post_id.getString("dt_txt");
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                c.setTime(format.parse(jsonDate));
                if (c.get(Calendar.HOUR_OF_DAY) == 15 && weatherPerDate.size() < 4) {
                    weatherPerDate.add(post_id);
                }
            }
            for (JSONObject obj : weatherPerDate) {
                JSONObject webArr = obj.getJSONArray("weather").getJSONObject(0);
                JSONObject tempArr = obj.getJSONObject("main");
                String jsonDate = obj.getString("dt_txt");
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                c.setTime(format.parse(jsonDate));

                weatherPerCity.add(new CityWeatherDto(c.get(Calendar.DAY_OF_WEEK), tempArr.getInt("temp"), webArr.getString("main")));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return weatherPerCity;
    }
}
