package com.awtServices;

import com.awtDao.TestemonyDao;
import com.awtDto.TestemonyDto;
import org.springframework.stereotype.Service;

import javax.persistence.Table;
import java.util.List;

@Service
@Table(name = "Testemony")
public class TestemonyService {

    public List<TestemonyDto> getTestemonies(TestemonyDao testemonyDao) {
        return testemonyDao.findAll();
    }
}
