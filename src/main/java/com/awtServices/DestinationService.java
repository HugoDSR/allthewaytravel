package com.awtServices;

import com.awtDao.DestinationDao;
import com.awtDto.DestinationDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Service
public class DestinationService {
    
    public List<DestinationDto> getDestinations(DestinationDao destinationDao) {
        return destinationDao.findAll();
    }


}
