package com.awtServices;

import com.awtDao.DestinationDao;
import com.awtDto.DestinationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@Service
public class ContactUsService {

    public void SendBudget(Map<String, String> emailContent, Environment env) {
        Properties props = System.getProperties();
        String to = env.getProperty("email.to");
        String from = env.getProperty("email.user");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Authenticator auth = new Authenticator() {
            //override the getPasswordAuthentication method
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, env.getProperty("email.pwd"));
            }
        };

        Session session = Session.getInstance(props, auth);
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject("Orcamento para "+ emailContent.get("name"));
            message.setText(buildEmailBody(emailContent));
            Transport.send(message);
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }

    private String buildEmailBody(Map<String, String> emailContent){
        return "Orcamento enviado para "+emailContent.get("name")+" - "+ emailContent.get("email")+" para as datas "+emailContent.get("goDay")+"/"+emailContent.get("goMonth")+
                "/"+emailContent.get("goYear")+" ate "+emailContent.get("comeDay")+"/"+emailContent.get("comeMonth")+"/"+emailContent.get("comeYear")+
                " para "+emailContent.get("passengerNumber")+" passageiros. Pretendem gastar por volta de "+emailContent.get("budget")+" e tem as seguintes"
                +" observacoes: "+emailContent.get("obsevation");
    }

}