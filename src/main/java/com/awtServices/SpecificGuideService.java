package com.awtServices;

import com.awtDao.SpecificGuideDao;
import com.awtDto.SpecificGuideDto;
import org.springframework.stereotype.Service;

@Service
public class SpecificGuideService {

    public SpecificGuideDto getSpecificGuide(SpecificGuideDao specificGuideDao, String cidadeName) {
        return specificGuideDao.findByNome(cidadeName);
    }
}
