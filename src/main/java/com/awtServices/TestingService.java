package com.awtServices;

import com.awtDao.UserDao;
import com.awtDto.UserDto;
import org.springframework.stereotype.Service;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

@Service
public class TestingService {

    public void testHashing(UserDto user) throws NoSuchAlgorithmException, NoSuchProviderException {


        String securePassword = getSecurePassword("mbbbggqv33", user.getSalt());

        System.out.println(user.getPassword());
        System.out.println(securePassword);
        System.out.println("Match?");
        System.out.println(securePassword.equals(user.getPassword()));

    }

    private static String getSecurePassword(String passwordToHash, byte[] salt)
    {
        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(salt);
            //Get the hash's bytes
            byte[] bytes = md.digest(passwordToHash.getBytes());
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }

    //Add salt
    private byte[] getSalt() throws NoSuchAlgorithmException, NoSuchProviderException
    {
        //Always use a SecureRandom generator
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "SUN");
        //Create array for salt
        byte[] salt = new byte[16];
        //Get a random salt
        sr.nextBytes(salt);
        //return salt
        return salt;
    }

    public UserDto createUser(String username, String password){
        UserDto user = new UserDto();
        byte [] salt = null;
        String securePassword = new String();
        try {
            salt = getSalt();
            securePassword = getSecurePassword(password, salt);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        user.setUsername(username);
        user.setPassword(securePassword);
        user.setSalt(salt);
        return user;
    }
}



//    // create and set properties into properties object
//    Properties props = new Properties();
//            props.setProperty("aboutUs."+topic, count);
//                    // get or create the file
//                    File f = new File("src/main/resources/application.properties");
//                    OutputStream out = new FileOutputStream( f );
//                    // write into it
//                    DefaultPropertiesPersister p = new DefaultPropertiesPersister();
//                    p.store(props, out, "Update Count");
