package com.awtServices;

import com.awtDao.TravelGuideDao;
import com.awtDto.RegionSelectionDto;
import com.awtDto.TravelGuideDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class TravelGuideService {

    public List<TravelGuideDto> getTravelGuides(TravelGuideDao travelGuideDao) {
        return  travelGuideDao.findAll();
    }

    public List<TravelGuideDto> getFilteredTravelGuides(TravelGuideDao travelGuideDao, List<String> selected){
        List<TravelGuideDto> result;
        if(selected.isEmpty() || selected.get(0).equalsIgnoreCase("")) {
            result = travelGuideDao.findAll();
        } else {
            result = travelGuideDao.findByWorldareaIn(selected);
        }
        return result;
    }

    public List<RegionSelectionDto> getRegionSelection(TravelGuideDao travelGuideDao, List<String> selected){
        List<String> travelOptions  = Arrays.asList("Europa", "América do Norte", "América Central", "Caraíbas",
                "América do Sul", "África", "Médio Oriente", "Ásia", "Oceânia", "Antártida");
        List<RegionSelectionDto> result = new ArrayList<>();
        List<String> regionsList = new ArrayList<>();
        for(TravelGuideDto val : travelGuideDao.findAll()){
            if(!regionsList.contains(val.getWorldarea())){
                regionsList.add(val.getWorldarea());
            }
        }
        for (String region : regionsList){
            result.add(new RegionSelectionDto(region, selected.contains(region)));
        }
        return result;
    }
}
