package com.awtDao;

import com.awtDto.SpecificGuideLocationDto;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SpecificGuideLocationDao extends CrudRepository<SpecificGuideLocationDto, Long> {
    @Override
    List<SpecificGuideLocationDto> findAll();

    List<SpecificGuideLocationDto> findByCity(String city);

}
