package com.awtDao;

import com.awtDto.TravelDataDto;
import com.awtDto.UserDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends CrudRepository<UserDto, Long> {

    UserDto findById(int id);

    UserDto save(UserDto userDto);
}