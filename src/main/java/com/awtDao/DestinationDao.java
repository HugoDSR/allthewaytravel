package com.awtDao;

import com.awtDto.DestinationDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface DestinationDao extends CrudRepository<DestinationDto, Long> {

    @Override
    List<DestinationDto> findAll();
}