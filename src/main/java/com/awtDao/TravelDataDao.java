package com.awtDao;

import com.awtDto.TravelDataDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TravelDataDao extends CrudRepository<TravelDataDto, Long> {

    TravelDataDto findById(int id);

    TravelDataDto save(TravelDataDto travelDAta);
}
