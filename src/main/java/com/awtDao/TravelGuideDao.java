package com.awtDao;

import com.awtDto.TravelGuideDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TravelGuideDao extends CrudRepository<TravelGuideDto, Long> {
    @Override
    List<TravelGuideDto> findAll();

    List<TravelGuideDto> findByWorldareaIn(List<String> worldareas);

}
