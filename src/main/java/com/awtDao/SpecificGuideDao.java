package com.awtDao;

import com.awtDto.SpecificGuideDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SpecificGuideDao extends CrudRepository<SpecificGuideDto, Long> {

    @Override
    List<SpecificGuideDto> findAll();

    SpecificGuideDto findByNome(String nome);
}
