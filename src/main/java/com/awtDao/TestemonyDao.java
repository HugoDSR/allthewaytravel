package com.awtDao;

import com.awtDto.TestemonyDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TestemonyDao extends CrudRepository<TestemonyDto, Long> {

    @Override
    List<TestemonyDto> findAll();
}
