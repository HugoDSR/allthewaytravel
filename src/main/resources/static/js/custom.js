$(document).ready(function() {
    $('#featured_guides').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        arrows: false,
        pauseOnHover: false
    });
    $('#featured_places').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        arrows: false,
        pauseOnHover: false
    });
    $('#recent-travel-guides').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        arrows: false,
        pauseOnHover: false
    });
    $('#most-read-travel-guides').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        arrows: false,
        pauseOnHover: false
    });
    $('#featured_testemony').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: false,
        pauseOnHover: false
    });


    $(document).click(function(event) {
        //if you click on anything except the modal itself or the "open modal" link, close the modal
        if (!$(event.target).closest(".modal,.close").length) {
            $("body").find(".modal").removeClass("visible");
        }
    });
    $(window).on('load resize', function(){
        if(document.getElementById('header-row') != null){
            var margin = document.getElementById('header-row').offsetHeight+2;
            document.getElementById('mainPage1').style.marginTop = margin.toString() + "px";
        }
     });

     $(document).on("click",".region-button", function () {
        var clickedBtnID = $(this).attr('id'); // or var clickedBtnID = this.id
        var element = document.getElementById(clickedBtnID.toString());
        var values = document.getElementById('filter-values').value;
        values = values.replace(clickedBtnID, "");
        if(element.classList.contains('selected')) {
            element.className = 'region-button';
            element.name = '';
            document.getElementById('filter-values').value = values;
         } else {
            element.className += ' selected';
            element.name = 'selected';
            document.getElementById('filter-values').value += clickedBtnID+'-';
         }
     });
    var mapLocation = document.getElementById('mapLocation').value.split(",");
    var mapLocationCoordinates = document.getElementById('mapLocationCoordinates').value.split(" - ");
//    var image = 'https://s3.eu-west-2.amazonaws.com/atwt/allthewaytravelPinContrast1.svg'
    var image = {
        url: "/images/allthewaytravelPin.svg", // url
        scaledSize: new google.maps.Size(50, 50), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(0, 0) // anchor
    };
    var map = new google.maps.Map(document.getElementById('location-google-map'), {
        zoom: 12,
        center: new google.maps.LatLng(Number(mapLocation[0]), Number(mapLocation[1]))
    });

    for (var i=0; i< mapLocationCoordinates.length-1; i++){
        var mapLocation = mapLocationCoordinates[i].split("*");
        var mapLocationCoordinate = mapLocation[0].split(", ")
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(Number(mapLocationCoordinate[0]), Number(mapLocationCoordinate[1])),
            map: map,
            icon: image,
            title: mapLocation[1]});
    }
});