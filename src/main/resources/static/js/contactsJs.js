$(document).ready(function() {
    var image = {
        url: "/images/allthewaytravelPin.svg", // url
        scaledSize: new google.maps.Size(50, 50), // scaled size
        origin: new google.maps.Point(0,0), // origin
        anchor: new google.maps.Point(0, 0) // anchor
    };

    var map = new google.maps.Map(document.getElementById('contacts-google-map'),{
    zoom: 8,
    center: new google.maps.LatLng(40.5921777,-8.6080003)
    });
    var marker = new google.maps.Marker({position: new google.maps.LatLng(41.1547868,-8.6634881), map: map, icon: image});
    var marker = new google.maps.Marker({position: new google.maps.LatLng(41.1183552,-8.6085897), map: map, icon: image});
    var marker = new google.maps.Marker({position: new google.maps.LatLng(40.6311714,-8.6480486), map: map, icon: image});
    var marker = new google.maps.Marker({position: new google.maps.LatLng(40.3712397,-8.4529043), map: map, icon: image});
    var marker = new google.maps.Marker({position: new google.maps.LatLng(40.35004,-8.5865815), map: map, icon: image});
});