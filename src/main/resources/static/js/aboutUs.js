$(document).ready(function() {

    var clients = document.getElementById('numberClientes').innerHTML;
    function incrementClientes() {
        clients++;
        document.getElementById('numberClientes').innerHTML = clients;
    }
    var voos = document.getElementById('numberVoos').innerHTML;
    function incrementVoos() {
        voos++;
        document.getElementById('numberVoos').innerHTML = voos;
    }
    var viagens = document.getElementById('numberViagens').innerHTML;
    function incrementViagens() {
        viagens++;
        document.getElementById('numberViagens').innerHTML = viagens;
    }
    var cruzeiros = document.getElementById('numberCruzeiros').innerHTML;
    function incrementCruzeiros() {
        cruzeiros++;
        document.getElementById('numberCruzeiros').innerHTML = cruzeiros;
    }

});